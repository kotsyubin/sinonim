import std.stdio;
import std.getopt;
import std.array;
import std.file;
import toml;
import mysql;
import vibe.vibe;

import myapp;

int main(string[] args) {
    string configPath;
    GetoptResult result;
    try {
        result = getopt(
            args,
            std.getopt.config.required,
            "config|c", "Path to application configuration file", &configPath
        );
    } catch (GetOptException e) {
        writefln("Bad cmdline args: %s", e.msg);
        return -1;
    }
    if (result.helpWanted) {
        defaultGetoptPrinter("Static site generator for user profilecards", result.options);
        return 0;
    }
    string config;
    try {
        config = readText(configPath);
        writefln("Got config from: %s",configPath);
    } catch (Exception e) {
        writeln(e.msg);
        return -1;
    }
    auto app_config = parseTOML(config);
    string db_path = join([app_config["database"]["toml"]["path"].str,app_config["database"]["toml"]["file"].str]);
    // auto selectDataProvider() {
    //     if (doc[`database`][`provider`] == "toml") {
    //         auto toml_db = parseTOML(join([doc[`database`][`toml`][`path`].str,doc[`database`][`toml`][`file`].str]));
    //         return toml_db;
    //     }

    // }
    // auto con_str = join((["host=",doc[`database`][`host`].str,";port=",doc[`database`][`port`].str,";user=",doc[`database`][`user`].str,";pwd=",doc[`database`][`password`].str,";db=",doc[`database`][`name`].str]));
    // writeln(con_str);
    // auto con = new Connection(con_str);
    // writeln(query(con,"SELECT * FROM sinonim.users;"));
    // writeln(query(con,"SELECT * FROM sinonim.services;"));
	// scope(exit) con.close();
    /* class Database {
        void initSelf() {
            string db_path;
            auto db = parseTOML(db_path);
            void getUsers() {
                auto db_users = db["users"];
            }
            void getServices() {
                auto db_services = db["services"];
            }
        }
    }
    Database app_db = Database.initSelf(db_path);*/
            auto router = new URLRouter;
            router.get("*", serveStaticFiles("public/"));
            router.registerWebInterface(new AppMetalinker);  

    listenHTTP(join([app_config[`app`][`host`].str,":",app_config[`app`][`port`].str]),router);
    runApplication();
    writeln("Terminating application...");
    return 0;
}

